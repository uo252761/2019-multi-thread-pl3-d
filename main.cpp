/*
 * Main.cpp
 *
 *  Created on: Fall 2019
 */

#include <stdio.h>
#include <math.h>
#include <CImg.h>
#include <time.h>
#include <pthread.h>
#include <math.h>
using namespace cimg_library;

// Data type for image components
// FIXME: Change this type according to your group assignment
typedef float data_t;

const char* SOURCE_IMG      = "bailarina.bmp";
const char* DESTINATION_IMG = "bailarina2.bmp";

int NHilos = 0;
	data_t *pRsrc, *pGsrc, *pBsrc; // Pointers to the R, G and B components
	data_t *pRdest, *pGdest, *pBdest;
	data_t *pDstImage; // Pointer to the new image pixels
	uint width, height; // Width and height of the image
	uint nComp; // Number of image components

void *task(void *x_void_ptr) {
	
	int inicio = *((int *)x_void_ptr);
	
	double c;
		c=pow(((100+50)/100),2);
	
	for (uint i=inicio; i< (uint) inicio + (height*width)/NHilos; i++){
		
        //Tocando el contraste
        *(pRdest + i) = *(pRsrc + i) * c;  // Esto es igual que pRdest[i] = pRsrc[i] * C
		*(pGdest + i) = *(pGsrc + i) * c;  // Esto es igual que pGdest[i] = pGsrc[i] * C
		*(pBdest + i) = *(pBsrc + i) * c;  // Esto es igual que pBdest[i] = pBsrc[i] * C

		//Convertir a Sepia
		*(pRdest + i) = *(pRsrc + i)  * 0.393 + *(pGsrc + i) * 0.769 + *(pBsrc + i) * 0.189;
		*(pGdest + i) = *(pRsrc + i)  * 0.349 + *(pGsrc + i) * 0.686 + *(pBsrc + i) * 0.168;
		*(pBdest + i) = *(pRsrc + i)  * 0.272 + *(pGsrc + i) * 0.534 + *(pBsrc + i) * 0.131;
		
		if (*(pRdest + i) > 255)
			*(pRdest + i) = 255;

		if (*(pGdest + i) > 255)
			*(pGdest + i) = 255;

		if (*(pBdest + i) > 255)
			*(pBdest + i) = 255;	
	}
	return NULL;

}

int main() {
	// Open file and object initialization
	
	CImg<data_t> srcImage(SOURCE_IMG);
	NHilos = 4;

	struct timespec tStart, tEnd;
    double dElapsedTimeS;


	//srcImage.display(); // Displays the source image
	width  = srcImage.width(); // Getting information from the source image
	height = srcImage.height();
	nComp  = srcImage.spectrum(); // source image number of components
				// Common values for spectrum (number of image components):
				//  B&W images = 1
				//	Normal color images = 3 (RGB)
				//  Special color images = 4 (RGB and alpha/transparency channel)


	// Allocate memory space for destination image components
	pDstImage = (data_t *) malloc (width * height * nComp * sizeof(data_t));
	if (pDstImage == NULL) {
		perror("Allocating destination image");
		exit(-2);
	}

	// Pointers to the componet arrays of the source image
	pRsrc = srcImage.data(); // pRcomp points to the R component array
	pGsrc = pRsrc + height * width; // pGcomp points to the G component array
	pBsrc = pGsrc + height * width; // pBcomp points to B component array

	// Pointers to the RGB arrays of the destination image
	pRdest = pDstImage;
	pGdest = pRdest + height * width;
	pBdest = pGdest + height * width;

	int errno;
	errno = clock_gettime(CLOCK_REALTIME, &tStart);
	
	if (errno == -1){
		printf ("error del tiempo de inicio");
		return -1;
	}

	/* this variable is our reference to the second thread */
	pthread_t multi[NHilos];
	int inicio[NHilos];
	
	for(int i = 0; i<NHilos;i++){
		inicio[i] = i*((height*width)/NHilos);
	}
	/* create a second thread which executes multi(&x) */
	for(int j=0; j< 15; j++){
		for(int i = 0; i<NHilos;i++){
		if(pthread_create(&multi[i], NULL, task, &inicio[i])) {

		fprintf(stderr, "Error creating thread\n");
		return 1;

	}
	}
	
	fflush (stdout);
	for(int i = 0; i<NHilos;i++){
	if(pthread_join(multi[i], NULL)) {

		fprintf(stderr, "Error joining thread\n");
		return 2;
	}
	}
	}
	
	errno = clock_gettime(CLOCK_REALTIME, &tEnd);

	if (errno == -1){
		printf ("error tiempo de fin");
		return -1;
	}

	dElapsedTimeS = (tEnd.tv_sec - tStart.tv_sec);
    	dElapsedTimeS += (tEnd.tv_nsec - tStart.tv_nsec) / 1e+9;

	printf("Elapsed time : %f s.\n", dElapsedTimeS);

	// Create a new image object with the calculated pixels
	// In case of normal color images use nComp=3,
	// In case of B/W images use nComp=1.
	CImg<data_t> dstImage(pDstImage, width, height, 1, nComp);

	// Store destination image in disk
	dstImage.save(DESTINATION_IMG);

	// Display destination image
	dstImage.display();
	return 0;
}

